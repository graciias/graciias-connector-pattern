package com.graciias.connector.test.utils;

import com.graciias.connector.entity.GraciiasAddress;
import com.graciias.connector.entity.GraciiasUser;

public class TestUtils {

    public static String EMAIL_USER_1 = "t.komminou@tiroutou.net";

    public static GraciiasUser getTestGraciiasUser() {
        GraciiasUser result = new GraciiasUser();
        result.setFirstName("Nicolas");
        result.setLastName("Noroutaux");
        result.setCompanyEmail(EMAIL_USER_1);
        GraciiasAddress graciiasAddress = new GraciiasAddress();
        graciiasAddress.setStreet("5 rue Vulpian");
        graciiasAddress.setRegion("Haut de seine");
        graciiasAddress.setPinCode("92100");
        graciiasAddress.setCountry("France");
        result.setAddress(graciiasAddress);
        return result;
    }

}
