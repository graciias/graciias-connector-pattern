package com.graciias.connector.test;

import com.graciias.connector.credentials.GraciiasCredentials;
import com.graciias.connector.entity.GraciiasUser;
import com.graciias.connector.entity.GraciiasUserBasic;
import com.graciias.connector.entity.GraciiasUserGroup;
import com.graciias.connector.exception.ConnectorException;
import com.graciias.connector.exception.GroupNotFoundException;
import com.graciias.connector.exception.UserAlreadyExists;
import com.graciias.connector.exception.UserNotFoundException;
import com.graciias.connector.pattern.GraciiasConnectorWithAddUser;
import com.graciias.connector.test.server.ServerTest;
import com.graciias.connector.test.utils.TestUtils;
import org.junit.jupiter.api.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ConnectorTest extends ServerTest {

    private GraciiasCredentials graciiasCredentials;

    private GraciiasConnectorWithAddUser connector;


    public ConnectorTest(GraciiasCredentials graciiasCredentials, GraciiasConnectorWithAddUser connector) {
        this.graciiasCredentials = graciiasCredentials;
        this.connector = connector;
    }

    @Order(1)
    @Test
    public void testAddUserRegularCase() throws ConnectorException {
        GraciiasUser testUser = TestUtils.getTestGraciiasUser();
        GraciiasUser response = connector.addUser(testUser, "testpassword", graciiasCredentials);
        Assertions.assertNotNull(response);
        Assertions.assertEquals(testUser.getCompanyEmail(), response.getCompanyEmail());
        Assertions.assertEquals(testUser.getFirstName(), response.getFirstName());
        Assertions.assertEquals(testUser.getLastName(), response.getLastName());
    }

    @Order(2)
    @Test
    public void testAddSameUser() throws ConnectorException, InterruptedException {
        Thread.sleep(1200);
        GraciiasUser testUser = TestUtils.getTestGraciiasUser();
        Assertions.assertThrows(UserAlreadyExists.class, () -> {
            connector.addUser(testUser, "testpassword2", graciiasCredentials);
        });
    }

    @Order(3)
    @Test
    public void testAddUserToNonExistingGroup() {
        GraciiasUser testUser = TestUtils.getTestGraciiasUser();
        GraciiasUserGroup nonExistingGroup = new GraciiasUserGroup();
        nonExistingGroup.setDescription("description");
        nonExistingGroup.setName("titkok");
        testUser.setGraciiasUserGroup(Arrays.asList(nonExistingGroup));
        Assertions.assertThrows(GroupNotFoundException.class, () -> {
            connector.addUser(testUser, "testpassword2", graciiasCredentials);
        });
    }

    @Order(4)
    @Test
    public void testGetUserDetails() throws ConnectorException {
        GraciiasUser testUser = TestUtils.getTestGraciiasUser();
        GraciiasUser response = connector.getUser(testUser.getCompanyEmail(), graciiasCredentials);
        Assertions.assertNotNull(response);
        Assertions.assertEquals(testUser.getCompanyEmail(), response.getCompanyEmail());
        Assertions.assertEquals(testUser.getFirstName(), response.getFirstName());
        Assertions.assertEquals(testUser.getLastName(), response.getLastName());
    }

    @Order(5)
    @Test
    public void testChangePassword() throws ConnectorException {
        String NEW_TEST_PASSWORD = "newTestPassword";
        GraciiasUser testUser = TestUtils.getTestGraciiasUser();
        String passwordResponse = connector.changePassword(testUser.getCompanyEmail(), NEW_TEST_PASSWORD, graciiasCredentials);
        Assertions.assertEquals(NEW_TEST_PASSWORD, passwordResponse);
    }

    @Order(6)
    @Test
    public void testUpdateUser() throws ConnectorException {
        GraciiasUser testUser = TestUtils.getTestGraciiasUser();
        testUser.setFirstName("Nicolas");
        testUser.setLastName("Cailloux");
        GraciiasUser updatedUser = connector.updateUser(testUser.getCompanyEmail(), testUser, graciiasCredentials);
        Assertions.assertNotNull(updatedUser);
        Assertions.assertEquals(updatedUser.getFirstName(), "Nicolas");
        Assertions.assertEquals(updatedUser.getLastName(), "Cailloux");
    }


    @Order(7)
    @Test
    public void testDeleteUserRegularCase() throws ConnectorException {
        connector.removeUser(TestUtils.getTestGraciiasUser(), graciiasCredentials);
    }

    @Order(8)
    @Test
    public void testDeleteUserWithUserNotFound() {
        Assertions.assertThrows(UserNotFoundException.class, () -> {
            connector.removeUser(TestUtils.getTestGraciiasUser(), graciiasCredentials);
        });
    }

    @Order(9)
    @Test
    public void testListUserRegularCase() throws ConnectorException {
        List<GraciiasUserBasic> userBasics = connector.getUserBasics(graciiasCredentials);
        List<String> userIds = userBasics.stream().map(x -> x.getId()).collect(Collectors.toList());
        GraciiasUser testUser = TestUtils.getTestGraciiasUser();
        Assertions.assertFalse(userIds.contains(testUser.getCompanyEmail()));
        Assertions.assertTrue(userIds.size() > 0);
    }

    @Order(10)
    @Test
    public void testListGroupRegularCase() throws ConnectorException {
        List<String> groupIds = connector.getGroupNames(graciiasCredentials);
        for (String groupId: groupIds) {
            System.out.println("groupId = " + groupId);
        }
        Assertions.assertTrue(groupIds.size() > 0);
    }

}
