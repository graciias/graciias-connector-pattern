package com.graciias.connector.test;

import com.graciias.connector.google.bean.GoogleConnector;
import com.graciias.connector.google.credentials.GoogleCredentials;
import org.springframework.beans.factory.annotation.Autowired;


public class GoogleConnectorTest extends ConnectorTest {

    @Autowired
    public GoogleConnectorTest(GoogleCredentials googleCredentials, GoogleConnector googleConnector) {
        super(googleCredentials.getGraciiasCredentialsForGoogle(), googleConnector);
    }
}
