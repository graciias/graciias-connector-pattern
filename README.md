# graciias-connector-pattern

###Architecture of the project
The project is based on a spring boot technology. It is composed of three maven modules: 

. The connector pattern module that contains all the graciias entity and interface that needs to be implemented

. The server module used for both the tests and to launch the server. For every connector, there is a need for a corresponding test

. The google connector module that is the connector that is using the google API. It is given as an example of the graciias interface use

###Start the test
In order to launch the test, first launch the command: `mvn clean install` on the global project.
Then directly start  the corresponding test class (The tests are made using Junit). 
A SpringBoot instance is then launch on a random port and the test are executed on it

###Authorization of the connector
The property file of the project follow the Spring boot default pattern. 
The application-test.properties is the file used only in the test. That is where the API identification parameter can be found.
For every connector, an account is created by Graciias on the application and a property is added on the test on format:

connector.account.test."name of the api".api_key=JGFDRREKFDKG4566MSS 

For some API, An OAuth2 mechanism can be required (or any two part authentication mechanism that can't be directly coded because
it requires a human interaction). In this case, the token can be put in the properties. 

**An md file (meaning a description file)
should be added to the module for describing the authentication mechanism**.  

###Code of a connector
A bean should implement a GraciiasConnector interface. That bean is the only class that will be used outside of this module

The developer should separate the code in four kind of class:
- The connector bean that implement one of the GraciiasConnector interface and the corresponding function
- The API entities that should correspond to the connector API expectation
- The parser that should parse the Graciias entities into an API entity or parse the API entity into a graciias entity
- A property class in order to access the property file information

If an http call has to be made in the process, the developer must use the Apache HTTP Client library (so there are no 
 different library for each module that do the same thing).
The entity parsing code should be in a parsing class and not directly in the function of a bean. 
 
###Versioning
When working on a new connector, the developer should work on a branch named connector/"connector name" and only on that
branch. He should never commit on the master branch or merge.

##Build maven
When creating a new connector, a new maven module should be added to the project. The package of this module should start with 
"com.graciias" so that the bean are injected on the server.


