package com.graciias.connector.pattern;

import com.graciias.connector.credentials.GraciiasCredentials;
import com.graciias.connector.entity.GraciiasUser;
import com.graciias.connector.entity.GraciiasUserBasic;
import com.graciias.connector.exception.ConnectorException;

import java.util.List;

public interface GraciiasConnector {


    /**
     * Get the list of all graciias user used by the client
     *
     * @return The list of graciias user used by the client
     * @throws ConnectorException
     */
    List<GraciiasUserBasic> getUserBasics(GraciiasCredentials credentials) throws ConnectorException;

    /**
     * Get the user entity corresponding to the id
     *
     * @param userId the id of the user we want the enity of (the id of the API, not the Graciias one)
     * @return the corresponding user entity
     * @throws ConnectorException
     */
    GraciiasUser getUser(String userId, GraciiasCredentials credentials) throws ConnectorException;


}
