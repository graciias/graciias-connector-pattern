package com.graciias.connector.pattern;

import com.graciias.connector.credentials.GraciiasCredentials;
import com.graciias.connector.entity.GraciiasUser;
import com.graciias.connector.exception.ConnectorException;

import java.util.List;

/**
 * This interface is used for the API that are like github, you can't create a new user from the administrator account,
 * but you can add it to the
 */
public interface GraciiasConnectorWithAddUser extends GraciiasConnector {

    /**
     *  Function used to add a user to one of the company group
     *
     * @param graciiasUser The user we are adding to the company group
     * @param password
     * @return the user entity that has been added to the group
     *
     * @throws ConnectorException
     */
    GraciiasUser addUser(GraciiasUser graciiasUser, String password, GraciiasCredentials credentials) throws ConnectorException;

    /**
     *
     * Function used to remove a user from a group
     *
     * @param graciiasUser The user we are trying to remove from the group
     * @throws ConnectorException
     */
    void removeUser(GraciiasUser graciiasUser, GraciiasCredentials credentials) throws ConnectorException;

    /**
     *
     * Function used to get the list of names of group of the client
     *
     * @return The list of names of the group
     * @throws ConnectorException
     */
    List<String> getGroupNames(GraciiasCredentials credentials) throws ConnectorException;

    /**
     * Function used to update the user entity
     *
     * @param userId The id of the user
     * @param graciiasUser The user we want to update
     * @return The new user entity
     * @throws ConnectorException
     */
    GraciiasUser updateUser(String userId, GraciiasUser graciiasUser, GraciiasCredentials credentials) throws ConnectorException;

    /**
     * Function used to re init the password of the user
     *
     * @param userId The id of the user
     * @param password The new password
     * @throws ConnectorException
     */
    String changePassword(String userId, String password, GraciiasCredentials credentials) throws ConnectorException;


}
