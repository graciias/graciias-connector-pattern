package com.graciias.connector.entity;

public enum GraciiasUserType {
    CONTRACTOR,
    EMPLOYEE,
    SHORT_TERM_EMPLOYEE
}
