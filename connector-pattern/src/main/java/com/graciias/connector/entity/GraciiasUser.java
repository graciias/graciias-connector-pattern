package com.graciias.connector.entity;

import org.joda.time.DateTime;

import java.util.List;

public class GraciiasUser {

    private GraciiasUserType userType;
    private String firstName;
    private String lastName;
    private String companyEmail;
    private String personalEmail;
    private GraciiasUser manager;
    private DateTime arrivalDate;
    private DateTime departureDate;
    private String function;
    private String service;
    private GraciiasSite graciiasSite;
    private List<GraciiasUserGroup> graciiasUserGroup;
    private Boolean active;
    private GraciiasAddress address;


    public GraciiasAddress getAddress() {
        return address;
    }

    public void setAddress(GraciiasAddress address) {
        this.address = address;
    }

    public GraciiasUserType getUserType() {
        return userType;
    }

    public void setUserType(GraciiasUserType userType) {
        this.userType = userType;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    public String getPersonalEmail() {
        return personalEmail;
    }

    public void setPersonalEmail(String personalEmail) {
        this.personalEmail = personalEmail;
    }

    public GraciiasUser getManager() {
        return manager;
    }

    public void setManager(GraciiasUser manager) {
        this.manager = manager;
    }

    public DateTime getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(DateTime arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public DateTime getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(DateTime departureDate) {
        this.departureDate = departureDate;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public GraciiasSite getGraciiasSite() {
        return graciiasSite;
    }

    public void setGraciiasSite(GraciiasSite graciiasSite) {
        this.graciiasSite = graciiasSite;
    }

    public List<GraciiasUserGroup> getGraciiasUserGroup() {
        return graciiasUserGroup;
    }

    public void setGraciiasUserGroup(List<GraciiasUserGroup> graciiasUserGroup) {
        this.graciiasUserGroup = graciiasUserGroup;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

}
