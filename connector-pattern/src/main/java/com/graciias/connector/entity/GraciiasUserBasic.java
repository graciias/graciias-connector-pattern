package com.graciias.connector.entity;

import org.joda.time.DateTime;

public class GraciiasUserBasic {

    private String id;

    private DateTime lastConnection;

    private String email;

    private Boolean active;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DateTime getLastConnection() {
        return lastConnection;
    }

    public void setLastConnection(DateTime lastConnection) {
        this.lastConnection = lastConnection;
    }
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }


}
