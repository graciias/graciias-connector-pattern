package com.graciias.connector.exception;

public class ConnectorException extends Exception {
    private Exception originalException;

    public ConnectorException(Exception originalException) {
        this.originalException = originalException;
    }

    public Exception getOriginalException() {
        return originalException;
    }

    public void setOriginalException(Exception originalException) {
        this.originalException = originalException;
    }

}
