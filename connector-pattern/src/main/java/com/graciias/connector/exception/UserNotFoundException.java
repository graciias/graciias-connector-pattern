package com.graciias.connector.exception;

public class UserNotFoundException extends ConnectorException {

    private String userNotFounded;

    public UserNotFoundException (String userNotFounded, Exception e) {
        super(e);
        this.userNotFounded = userNotFounded;
    }

}
