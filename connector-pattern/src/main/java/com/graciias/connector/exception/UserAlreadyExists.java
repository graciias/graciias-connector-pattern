package com.graciias.connector.exception;

public class UserAlreadyExists extends ConnectorException {

    private String userAlreadyExists;

    public UserAlreadyExists(String userAlreadyExists, Exception e) {
        super(e);
        this.userAlreadyExists = userAlreadyExists;
    }
}
