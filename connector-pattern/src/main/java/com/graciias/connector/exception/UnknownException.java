package com.graciias.connector.exception;

public class UnknownException extends ConnectorException {
    public UnknownException(Exception originalException) {
        super(originalException);
    }
}
