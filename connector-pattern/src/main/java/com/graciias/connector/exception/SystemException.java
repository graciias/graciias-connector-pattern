package com.graciias.connector.exception;

public class SystemException extends ConnectorException{
    public SystemException(Exception e) {
        super(e);
    }
}
