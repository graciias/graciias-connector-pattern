package com.graciias.connector.exception;

public class GroupNotFoundException extends ConnectorException {

    private String groupNotFounded;

    public GroupNotFoundException (String groupNotFounded, Exception originalException) {
        super(originalException);
        this.groupNotFounded = groupNotFounded;
    }
}
