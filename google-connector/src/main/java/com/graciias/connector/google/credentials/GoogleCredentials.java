package com.graciias.connector.google.credentials;

import com.graciias.connector.credentials.GraciiasCredentials;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class GoogleCredentials {

    @Value("${connector.account.test.google.certificate.password}")
    private String certificatePassword;

    @Value("${connector.account.test.google.certificate.location}")
    private String certificateLocation;

    @Value("${connector.account.test.google.account.id}")
    private String accountId;

    @Value("${connector.account.test.google.account.user}")
    private String userId;

    @Value("${connector.account.test.google.application.name}")
    private String applicationName;

    @Value("${connector.account.test.google.domain.name}")
    private String domainName;

    @Value("${connector.account.test.google.group.name}")
    private String testGroupName;

    public GraciiasCredentials getGraciiasCredentialsForGoogle() {
        GraciiasCredentials result = new GraciiasCredentials();
        result.setAccountId(accountId);
        result.setAccountUser(userId);
        result.setCertificateLocationFile(certificateLocation);
        result.setCertificatePassword(certificatePassword);
        result.setApplicationName(applicationName);
        result.setDomainName(domainName);
        result.setTestGroupName(testGroupName);
        return result;
    }
}
