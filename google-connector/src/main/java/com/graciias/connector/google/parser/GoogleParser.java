package com.graciias.connector.google.parser;

import com.google.api.services.admin.directory.model.User;
import com.google.api.services.admin.directory.model.UserAddress;
import com.google.api.services.admin.directory.model.UserName;
import com.graciias.connector.entity.GraciiasAddress;
import com.graciias.connector.entity.GraciiasUser;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class GoogleParser {

    public User getGoogleUserFromGraciiasUser(GraciiasUser graciiasUser) {
        User result = new User();
        // populate are the required fields only
        UserName name = new UserName();
        name.setFamilyName(graciiasUser.getLastName());
        name.setGivenName(graciiasUser.getFirstName());
        result.setName(name);
        result.setPrimaryEmail(graciiasUser.getCompanyEmail());
        if(graciiasUser.getAddress() != null) {
            UserAddress userAddress = new UserAddress();
            userAddress.setCountry(graciiasUser.getAddress().getCountry());
            userAddress.setRegion(graciiasUser.getAddress().getRegion());
            userAddress.setStreetAddress(graciiasUser.getAddress().getStreet());
            result.setAddresses(Arrays.asList(userAddress));
        }
        return result;
    }

    public GraciiasUser getGraciiasUserFromGoogleUser(User user) {
        GraciiasUser result = new GraciiasUser();
        result.setFirstName(user.getName().getGivenName());
        result.setLastName(user.getName().getFamilyName());
        result.setCompanyEmail(user.getPrimaryEmail());
        if(user.getAddresses() != null && !user.getAddresses().isEmpty()) {
            GraciiasAddress graciiasAddress = new GraciiasAddress();
            graciiasAddress.setCountry(user.getAddresses().get(0).getCountry());
            graciiasAddress.setRegion(user.getAddresses().get(0).getRegion());
            graciiasAddress.setStreet(user.getAddresses().get(0).getStreetAddress());
            result.setAddress(graciiasAddress);
        }
        return result;
    }

}
