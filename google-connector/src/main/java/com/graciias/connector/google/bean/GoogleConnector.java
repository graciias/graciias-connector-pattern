package com.graciias.connector.google.bean;


import com.google.api.services.admin.directory.model.User;
import com.graciias.connector.credentials.GraciiasCredentials;
import com.graciias.connector.entity.GraciiasUser;
import com.graciias.connector.entity.GraciiasUserBasic;
import com.graciias.connector.exception.*;
import com.graciias.connector.google.parser.GoogleParser;
import com.graciias.connector.google.utils.DirectoryUtils;
import com.graciias.connector.pattern.GraciiasConnectorWithAddUser;
import org.apache.commons.lang3.RandomStringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class GoogleConnector implements GraciiasConnectorWithAddUser {

    @Autowired
    private GoogleParser googleParser;

    @Autowired
    private DirectoryUtils directoryUtils;


    @Override
    public GraciiasUser addUser(GraciiasUser graciiasUser, String password, GraciiasCredentials credentials) throws ConnectorException {
        if(graciiasUser.getGraciiasUserGroup() != null && !graciiasUser.getGraciiasUserGroup().isEmpty()) {
            // we first make sure that all the groups do exists:
            List<String> allGroupNames = this.getGroupNames(credentials);
            for(String groupName: graciiasUser.getGraciiasUserGroup().stream().map(x -> x.getName()).collect(Collectors.toList())) {
                if(!allGroupNames.contains(groupName)) {
                   throw new GroupNotFoundException(groupName, null);
                }
            }
        }
        // we make sure that the user does not already exist
        List<GraciiasUserBasic> userBasics = this.getUserBasics(credentials);
        List<String> allUserIds = userBasics.stream().map(x -> x.getId()).collect(Collectors.toList());
        if(allUserIds.contains(graciiasUser.getCompanyEmail())) {
            throw new UserAlreadyExists(graciiasUser.getCompanyEmail(), null);
        }
        User user = googleParser.getGoogleUserFromGraciiasUser(graciiasUser);
        if(password != null) {
            user.setPassword(password);
        } else {
            user.setPassword(RandomStringUtils.random(15, true, true));
        }
        try {
            User response = directoryUtils.createUser(user, directoryUtils.initDirectory(credentials));
            return googleParser.getGraciiasUserFromGoogleUser(response);
        } catch (IOException e) {
            e.printStackTrace();
            throw new SystemException(e);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
            throw new SystemException(e);
        }
    }

    @Override
    public void removeUser(GraciiasUser graciiasUser, GraciiasCredentials credentials) throws ConnectorException {
        try {
            // we make sure that the user does exist
            List<GraciiasUserBasic> userBasics = this.getUserBasics(credentials);
            List<String> allUserIds = userBasics.stream().map(x -> x.getId()).collect(Collectors.toList());
            if(!allUserIds.contains(graciiasUser.getCompanyEmail())) {
                throw new UserNotFoundException(graciiasUser.getCompanyEmail(), null);
            }
            directoryUtils.deleteUser(graciiasUser.getCompanyEmail(), directoryUtils.initDirectory(credentials));
        } catch (IOException e) {
            e.printStackTrace();
            throw new SystemException(e);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
            throw new SystemException(e);
        }
    }

    @Override
    public List<String> getGroupNames(GraciiasCredentials credentials) throws ConnectorException {
        try {
            return directoryUtils.getAllGroups(directoryUtils.initDirectory(credentials), credentials.getDomainName());
        } catch (IOException e) {
            e.printStackTrace();
            throw new SystemException(e);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
            throw new SystemException(e);
        }
    }

    @Override
    public List<GraciiasUserBasic> getUserBasics(GraciiasCredentials credentials) throws ConnectorException {
        try {
            List<GraciiasUserBasic> result = new ArrayList<>();
            List<Map<String,Object>> userBasics = directoryUtils.getAllUserBasics(directoryUtils.initDirectory(credentials), credentials.getDomainName());
            for(Map<String,Object> userBasic : userBasics) {
                GraciiasUserBasic graciiasUserBasic = new GraciiasUserBasic();
                graciiasUserBasic.setEmail((String) userBasic.get("primaryEmail"));
                graciiasUserBasic.setId((String) userBasic.get("primaryEmail"));
                graciiasUserBasic.setLastConnection(new DateTime(((com.google.api.client.util.DateTime) userBasic.get("lastLoginTime")).getValue()));
                graciiasUserBasic.setActive(!(Boolean) userBasic.get("suspended"));
                result.add(graciiasUserBasic);
            }
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            throw new SystemException(e);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
            throw new SystemException(e);
        }
    }

    @Override
    public GraciiasUser getUser(String userId, GraciiasCredentials credentials) throws ConnectorException {
        try {
            User foundUser = directoryUtils.getUser(userId, directoryUtils.initDirectory(credentials));
            return googleParser.getGraciiasUserFromGoogleUser(foundUser);
        } catch (IOException e) {
            e.printStackTrace();
            throw new SystemException(e);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
            throw new SystemException(e);
        }
    }

    @Override
    public GraciiasUser updateUser(String userId, GraciiasUser graciiasUser, GraciiasCredentials credentials) throws ConnectorException {
        try {
            User response = directoryUtils.updateUser(userId, googleParser.getGoogleUserFromGraciiasUser(graciiasUser), directoryUtils.initDirectory(credentials));
            return googleParser.getGraciiasUserFromGoogleUser(response);
        } catch (IOException e) {
            e.printStackTrace();
            throw new SystemException(e);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
            throw new SystemException(e);
        }
    }

    @Override
    public String changePassword(String userId, String password, GraciiasCredentials credentials) throws ConnectorException {
        User changePassword = new User();
        changePassword.setPassword(password);
        try {
            User response = directoryUtils.updateUser(userId, changePassword, directoryUtils.initDirectory(credentials));
            return password;
        } catch (IOException e) {
            e.printStackTrace();
            throw new SystemException(e);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
            throw new SystemException(e);
        }
    }


}
