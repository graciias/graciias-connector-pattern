package com.graciias.connector.google.utils;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.admin.directory.Directory;
import com.google.api.services.admin.directory.DirectoryScopes;
import com.google.api.services.admin.directory.model.Groups;
import com.google.api.services.admin.directory.model.User;
import com.google.api.services.admin.directory.model.Users;
import com.graciias.connector.credentials.GraciiasCredentials;
import org.springframework.stereotype.Component;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class DirectoryUtils {

    /** Global instance of the JSON factory. */
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

    /** Global instance of the HTTP transport. */
    private static HttpTransport httpTransport;

    public Directory initDirectory(GraciiasCredentials graciiasCredentials) throws GeneralSecurityException, IOException {
        httpTransport = GoogleNetHttpTransport.newTrustedTransport();
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();
        System.out.println("Current relative path is: " + s);

        // You can specify a credential file by providing a path to GoogleCredentials.
        // Otherwise credentials are read from the GOOGLE_APPLICATION_CREDENTIALS environment variable.
        KeyStore keystore = KeyStore.getInstance("PKCS12");
        keystore.load(new FileInputStream(graciiasCredentials.getCertificateLocationFile()),  graciiasCredentials.getCertificatePassword().toCharArray());
        PrivateKey pk = (PrivateKey)keystore.getKey("privatekey",  graciiasCredentials.getCertificatePassword().toCharArray());

        GoogleCredential credential = new GoogleCredential.Builder()
                .setTransport(httpTransport)
                .setJsonFactory(jsonFactory)
                .setServiceAccountId(graciiasCredentials.getAccountId())
                .setServiceAccountScopes(Arrays.asList(DirectoryScopes.ADMIN_DIRECTORY_USER, DirectoryScopes.ADMIN_DIRECTORY_GROUP_READONLY)) //DirectoryScopes.ADMIN_DIRECTORY_GROUP_READONLY
                .setServiceAccountUser(graciiasCredentials.getAccountUser())
                .setServiceAccountPrivateKey(pk) //<----THIS
                .build();

        Directory directory = new Directory.Builder(httpTransport, JSON_FACTORY, credential)
                .setApplicationName(graciiasCredentials.getApplicationName())
                .build();

        return directory;
    }

    public void deleteUser(String email, Directory directory) throws IOException {
        directory.users().delete(email).execute();
    }

    public User createUser(User user ,Directory directory) throws IOException {
        User result = directory.users().insert(user).execute();
        return result;
    }

    public User updateUser(String userKey, User user ,Directory directory) throws IOException {
        User result = directory.users().update(userKey, user).execute();
        return result;
    }

    public List<String> getAllGroups(Directory directory, String domainName) throws IOException {
        Directory.Groups.List listReq = directory.groups().list();
        listReq.setDomain(domainName);
        Groups allGroups = listReq.execute();
        List<Map<String,String>> parsed = (List<Map<String,String>>) allGroups.get("groups");
        List<String> result = parsed.stream().map(x -> x.get("name")).collect(Collectors.toList());
        Collections.sort(result);
        return result;

    }

    /**
     *
     * Function used to get the
     *
     * @param directory
     * @param domainName
     * @return The user email and the last connection Datetime associated
     * @throws IOException
     */
    public List<Map<String,Object>> getAllUserBasics(Directory directory, String domainName) throws IOException {
        Directory.Users.List listReq = directory.users().list();
        listReq.setDomain(domainName);
        Users allUsers = listReq.execute();
        List<Map<String,Object>> result = (List<Map<String,Object>>) allUsers.get("users");
        return result;
    }

    public User getUser(String userId ,Directory directory) throws IOException {
        User user = directory.users().get(userId).execute();
        return user;
    }


}
