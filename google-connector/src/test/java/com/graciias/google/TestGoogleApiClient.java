package com.graciias.google;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.admin.directory.Directory;
import com.google.api.services.admin.directory.DirectoryScopes;
import com.google.api.services.admin.directory.model.User;
import com.google.api.services.admin.directory.model.UserName;
import org.junit.Assert;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.util.Arrays;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TestGoogleApiClient {

    private String USER_EMAIL = "t.komminou@tiroutou.net";

    @Test
    @Order(1)
    public void testGoogleUserAdd () {
        try {
            User user = this.createUser("Dupont","Gérard","titFsdfdsDDi44", USER_EMAIL, this.initDirectory());
            Assert.assertNotNull(user);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Order(2)
    public void testGoogleUserDelete () {
        try {
            this.deleteUser(USER_EMAIL, this.initDirectory());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
    }

    /** Global instance of the JSON factory. */
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

    /** Global instance of the HTTP transport. */
    private static HttpTransport httpTransport;

    public static Directory initDirectory() throws GeneralSecurityException, IOException {
        httpTransport = GoogleNetHttpTransport.newTrustedTransport();
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();
        System.out.println("Current relative path is: " + s);

        // You can specify a credential file by providing a path to GoogleCredentials.
        // Otherwise credentials are read from the GOOGLE_APPLICATION_CREDENTIALS environment variable.
        KeyStore keystore = KeyStore.getInstance("PKCS12");
        keystore.load(new FileInputStream("src/main/resources/credential/graciiastest-f4d86d6237d5.p12"), "notasecret".toCharArray());
        PrivateKey pk = (PrivateKey)keystore.getKey("privatekey", "notasecret".toCharArray());

        GoogleCredential credential = new GoogleCredential.Builder()
                .setTransport(httpTransport)
                .setJsonFactory(jsonFactory)
                .setServiceAccountId("administrator@graciiastest.iam.gserviceaccount.com")
                .setServiceAccountScopes(Arrays.asList(DirectoryScopes.ADMIN_DIRECTORY_USER))
                .setServiceAccountUser("test@tiroutou.net")
                .setServiceAccountPrivateKey(pk) //<----THIS
                .build();

        Directory directory = new Directory.Builder(httpTransport, JSON_FACTORY, credential)
                .setApplicationName("graciias")
                .build();

        return directory;
    }
    public static User createUser(String familyName, String givenName, String password, String primaryEmail,Directory directory) throws IOException {
        User user = new User();
        // populate are the required fields only
        UserName name = new UserName();
        name.setFamilyName(familyName);
        name.setGivenName(givenName);
        user.setName(name);
        user.setPassword(password);
        user.setPrimaryEmail(primaryEmail);

        // requires DirectoryScopes.ADMIN_DIRECTORY_USER scope
        user = directory.users().insert(user).execute();
        return user;
    }
    public static void deleteUser(String email, Directory directory) throws IOException {
        // requires DirectoryScopes.ADMIN_DIRECTORY_USER scope
        directory.users().delete(email).execute();
    }


}
